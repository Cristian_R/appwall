Template.form.events({
    'submit form': function (event) {
        event.preventDefault();
        var imageFile = event.currentTarget.children[0].children[0].children[0].files[0];


        if (imageFile == undefined) {
            Materialize.toast('Empty', 4000) // 4000 is the duration of the toast
        }



        Collections.Images.insert(imageFile, function (error, fileObject) {
            if (error) {
                alert("Fail");
            } else {
                //submit post data to database
                //Authour, Message
                //fileObject._id
                $('.grid').masonry('reloadItems');
            }


        });
    }
});

//Basic
//Milestone 1: Wireframe Design-Yes
//Milestone 2: Post Form-Yes
//Milestone 3: Functional Post Form-Yes
//Milestone 4: Form Data Validation-Yes
//Milestone 5: Display Post Text Data-No
//Milestone 6: Display Post Image Data-No
//Milestone 7: Organize Posts using Masonry-Yes
//Milestone 8: Styling-Yes
//Intermediate
//Milestone 1: Host Your Project Online-Yes
//Milestone 2: Sort Posts by Created Date-No
//Milestone 3: Add Gutters Between Posts-Yes
//Advanced
//Milestone 1: User Authentication-No
//Milestone 2: Delete Your Own Posts-No