"use strict";

Template.post.helpers({
    postList: function () {
        return Collections.Images.find({});
    },

    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true
                    //gutter:18;
            });
        });
    }
});
//Basic
//Milestone 1: Wireframe Design-Yes
//Milestone 2: Post Form-Yes
//Milestone 3: Functional Post Form-Yes
//Milestone 4: Form Data Validation-Yes
//Milestone 5: Display Post Text Data-No
//Milestone 6: Display Post Image Data-No
//Milestone 7: Organize Posts using Masonry-Yes
//Milestone 8: Styling-Yes
//Intermediate
//Milestone 1: Host Your Project Online-Yes
//Milestone 2: Sort Posts by Created Date-No
//Milestone 3: Add Gutters Between Posts-Yes
//Advanced
//Milestone 1: User Authentication-No
//Milestone 2: Delete Your Own Posts-No